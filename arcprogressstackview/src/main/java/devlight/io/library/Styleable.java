/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package devlight.io.library;

class Styleable {
    /**
     * 是否有动画
     */
    public static final String ANIMATED = "apsv_animated";

    /**
     * 是否需要阴影
     */
    public static final String SHADOWED = "apsv_shadowed";

    /**
     * 是否是圆角
     */
    public static final String ROUNDED = "apsv_rounded";

    /**
     * 是否可以手动移动进度条
     */
    public static final String DRAGGED = "apsv_dragged";

    /**
     * 进度条圆角的时候，设置是否在起始位置添加阴影
     */
    public static final String LEVELED = "apsv_leveled";

    /**
     * 字体
     */
    public static final String TYPEFACE = "apsv_typeface";

    /**
     * 文字颜色
     */
    public static final String TEXT_COLOR = "apsv_text_color";

    /**
     * 阴影偏移量
     */
    public static final String SHADOW_DISTANCE = "apsv_shadow_distance";

    /**
     * 阴影角度位置
     */
    public static final String SHADOW_ANGLE = "apsv_shadow_angle";

    /**
     * 阴影半径
     */
    public static final String SHADOW_RADIUS = "apsv_shadow_radius";

    /**
     * 阴影颜色
     */
    public static final String SHADOW_COLOR = "apsv_shadow_color";

    /**
     * 动画时长
     */
    public static final String ANIMATION_DURATION = "apsv_animation_duration";

    /**
     * 动画曲线类型
     */
    public static final String INTERPOLATOR = "apsv_interpolator";

    /**
     * 进度条的总宽度
     */
    public static final String DRAW_WIDTH = "apsv_draw_width";

    /**
     * 进度条之间的间隔
     */
    public static final String MODEL_OFFSET = "apsv_model_offset";

    /**
     * 是否显示背景
     */
    public static final String MODEL_BG_ENABLED = "apsv_model_bg_enabled";

    /**
     * 是否显示百分比文字
     */
    public static final String SHOW_PROGRESS = "apsv_show_progress";

    /**
     * 进度条开始的角度
     */
    public static final String START_ANGLE = "apsv_start_angle";

    /**
     * 进度条最大的角度
     */
    public static final String SWEEP_ANGLE = "apsv_sweep_angle";

    /**
     * 百分比文字的方向
     */
    public static final String INDICATOR_ORIENTATION = "apsv_indicator_orientation";

}
