package devlight.io.sample;

import com.gigamole.arcprogressstackview.ResourceTable;

import devlight.io.library.ArcProgressStackView;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;

import static devlight.io.library.ArcProgressStackView.Model;

import static devlight.io.sample.MainActivity.MODEL_COUNT;

/**
 * Created by GIGAMOLE on 9/21/16.
 */
public class PresentationActivity extends Ability {
    private int mCounter = 0;
    private ArcProgressStackView mArcProgressStackView;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_presentation);

        mArcProgressStackView = (ArcProgressStackView) findComponentById(ResourceTable.Id_apsv_presentation);
        mArcProgressStackView.setShadowColor(new Color(Color.argb(200, 0, 0, 0)));
        mArcProgressStackView.setAnimationDuration(1000);
        mArcProgressStackView.setSweepAngle(270);

        try {
            final String[] stringColors = getResourceManager().getElement(ResourceTable.Strarray_devlight).getStringArray();
            final String[] stringBgColors = getResourceManager().getElement(ResourceTable.Strarray_bg).getStringArray();
            final int[] colors = new int[MODEL_COUNT];
            final int[] bgColors = new int[MODEL_COUNT];
            for (int i = 0; i < MODEL_COUNT; i++) {
                colors[i] = Color.getIntColor(stringColors[i]);
                bgColors[i] = Color.getIntColor(stringBgColors[i]);
            }
            final ArrayList<ArcProgressStackView.Model> models = new ArrayList<>();
            models.add(new Model("STRATEGY", 1, new Color(bgColors[0]), new Color(colors[0])));
            models.add(new Model("DESIGN", 1, new Color(bgColors[1]), new Color(colors[1])));
            models.add(new Model("DEVELOPMENT", 1, new Color(bgColors[2]), new Color(colors[2])));
            models.add(new Model("QA", 1, new Color(bgColors[3]), new Color(colors[3])));
            mArcProgressStackView.setModels(models);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }

        final AnimatorValue valueAnimator = new AnimatorValue();
        valueAnimator.setDuration(800);
        valueAnimator.setDelay(200);
        valueAnimator.setLoopedCount(MODEL_COUNT - 1);
        valueAnimator.setLoopedListener(new Animator.LoopedListener() {
            @Override
            public void onRepeat(Animator animator) {
                mCounter++;
            }
        });
        valueAnimator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                mCounter = 0;
                for (final Model model : mArcProgressStackView.getModels()) {
                    model.setProgress(1);
                }
                mArcProgressStackView.animateProgress();
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        valueAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                mArcProgressStackView.getModels().get(Math.min(mCounter, MODEL_COUNT - 1))
                        .setProgress(1 + 104 * v);
                mArcProgressStackView.invalidate();
            }
        });

        mArcProgressStackView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (valueAnimator.isRunning()) {
                    return;
                }
                if (mArcProgressStackView.getProgressAnimator().isRunning()) {
                    return;
                }
                valueAnimator.start();
            }
        });
    }

}
