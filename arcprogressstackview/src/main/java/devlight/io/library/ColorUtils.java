/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package devlight.io.library;

/**
 * 颜色工具类
 */
public class ColorUtils {
    /**
     * 将int颜色转化成rgb
     *
     * @param argb 颜色值
     * @return float数组
     */
    public static float[] converArgbToRgb(int argb) {
        float[] rgb = new float[4];
        rgb[0] = (argb & 0xff0000) >> 16;
        rgb[1] = (argb & 0xff00) >> 8;
        rgb[2] = (argb & 0xff);
        rgb[3] = argb >>> 24;
        return rgb;
    }
}
